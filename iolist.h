#ifndef CTP_IOLIST_H
#define CTP_IOLIST_H

#include <cstddef>
#include <initializer_list>
#include <iterator>
#include <list>
#include <ostream>
#include <string>
#include <utility>

// iolist is a string building mechanism similar to std::stringstream.  It
// is inspired by the Erlang construct of the same name.  Its primary
// benefit over std::stringstream is that rvalues, including other iolists,
// can be concatenated to an iolist in constant time.  This behavior is
// particularly well-suited to applications which build strings recursively.

namespace ctp
{
  //
  // INTERFACE
  //

  // Guarantees:
  // * all methods concatenating rvalues execute in constant time, except where noted
  // * size is proportional to # of characters (empty strings aren't stored)
  // * all methods provide strong exception guarantee
  // * when copies are made, flattening is always performed

  template<typename charT, typename traits> class basic_iolist;

  // Write an iolist out to a stream.  Linear in the number of characters in the string.
  template<typename charT, typename traits> std::basic_ostream<charT, traits> &operator<<(
    std::basic_ostream<charT, traits> &, const basic_iolist<charT, traits> &);

  template<typename charT, typename traits> void swap(basic_iolist<charT, traits> &, basic_iolist<charT, traits> &) noexcept;

  template<typename charT, typename traits = std::char_traits<charT>> class basic_iolist
  {
  public:
    class iterator
    {
    public:
      using value_type = typename std::basic_string<charT, traits>::iterator::value_type;
      using difference_type = typename std::basic_string<charT, traits>::iterator::difference_type;
      using pointer = typename std::basic_string<charT, traits>::iterator::pointer;
      using reference = typename std::basic_string<charT, traits>::iterator::reference;
      using iterator_category = std::bidirectional_iterator_tag;

      iterator() = default;

      iterator &operator++();
      iterator operator++(int);
      iterator &operator--();
      iterator operator--(int);

      bool operator==(const iterator &) const;
      bool operator!=(const iterator &) const;

      reference operator*() const;
      pointer operator->() const;

    private:
      typename std::list<std::basic_string<charT, traits>>::iterator m_l, m_l_end;
      typename std::basic_string<charT, traits>::iterator m_s;

      iterator(typename std::list<std::basic_string<charT, traits>>::iterator,
        typename std::list<std::basic_string<charT, traits>>::iterator,
        typename std::basic_string<charT, traits>::iterator);

      friend class basic_iolist;
    };

    class const_iterator
    {
    public:
      using value_type = typename std::basic_string<charT, traits>::const_iterator::value_type;
      using difference_type = typename std::basic_string<charT, traits>::const_iterator::difference_type;
      using pointer = const typename std::basic_string<charT, traits>::const_iterator::pointer;
      using reference = const typename std::basic_string<charT, traits>::const_iterator::reference;
      using iterator_category = std::bidirectional_iterator_tag;

      const_iterator() = default;

      const_iterator &operator++();
      const_iterator operator++(int);
      const_iterator &operator--();
      const_iterator operator--(int);

      bool operator==(const const_iterator &) const;
      bool operator!=(const const_iterator &) const;

      reference operator*() const;
      pointer operator->() const;

    private:
      typename std::list<std::basic_string<charT, traits>>::const_iterator m_l, m_l_end;
      typename std::basic_string<charT, traits>::const_iterator m_s;

      const_iterator(typename std::list<std::basic_string<charT, traits>>::const_iterator,
        typename std::list<std::basic_string<charT, traits>>::const_iterator,
        typename std::basic_string<charT, traits>::const_iterator);

      friend class basic_iolist;
    };

    basic_iolist();
    basic_iolist(const charT *);
    basic_iolist(const charT *, std::size_t);
    basic_iolist(const std::basic_string<charT, traits> &);
    basic_iolist(std::basic_string<charT, traits> &&);
    basic_iolist(const basic_iolist &);
    basic_iolist(basic_iolist &&) noexcept = default;
    basic_iolist(std::initializer_list<basic_iolist>);

    basic_iolist &operator=(const basic_iolist &);
    basic_iolist &operator=(basic_iolist &&) noexcept = default;

    // Reset an iolist to the empty string.
    void clear() noexcept;
    // Check whether an iolist represents the empty string.
    bool empty() const noexcept;
    // Flatten the internal representation of the iolist, in order to reduce
    // memory fragmentation.  Linear in the number of characters in the iolist,
    // but repeated calls to flatten() do no further work.
    void flatten();

    // Append a string to the end of the iolist.
    basic_iolist &append(const charT *);
    basic_iolist &append(const charT *, std::size_t);
    basic_iolist &append(const std::basic_string<charT, traits> &);
    basic_iolist &append(std::basic_string<charT, traits> &&);
    basic_iolist &append(const basic_iolist &);
    basic_iolist &append(basic_iolist &&);
    basic_iolist &append(std::initializer_list<basic_iolist>);

    // Prepend a string to the beginning of the iolist.
    basic_iolist &prepend(const charT *);
    basic_iolist &prepend(const charT *, std::size_t);
    basic_iolist &prepend(const std::basic_string<charT, traits> &);
    basic_iolist &prepend(std::basic_string<charT, traits> &&);
    basic_iolist &prepend(const basic_iolist &);
    basic_iolist &prepend(basic_iolist &&);
    basic_iolist &prepend(std::initializer_list<basic_iolist>);

    // Replace the contents of the iolist with the given string.
    basic_iolist &assign(const charT *);
    basic_iolist &assign(const charT *, std::size_t);
    basic_iolist &assign(const std::basic_string<charT, traits> &);
    basic_iolist &assign(std::basic_string<charT, traits> &&);
    basic_iolist &assign(std::initializer_list<basic_iolist>);

    // Manipulate the last character of the iolist.
    void push_back(charT);
    void pop_back();
    charT &back();
    const charT &back() const;

    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;
    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }

    // Return a copy of the contents of the iolist as a string.  Linear
    // in the number of characters in the iolist.
    std::basic_string<charT, traits> str() const &;
    operator std::basic_string<charT, traits>() const & { return str(); }
    std::basic_string<charT, traits> str() &&;
    operator std::basic_string<charT, traits>() && { return std::move(*this).str(); }

  private:
    // invariants:
    // * list is never empty
    // * elements may not be empty unless there is only one
    std::list<std::basic_string<charT, traits>> m_l;

    static std::basic_string<charT, traits> flatten(std::initializer_list<basic_iolist>);

    friend std::basic_ostream<charT, traits> &operator<< <charT, traits>(std::basic_ostream<charT, traits> &, const basic_iolist &);
    friend void swap<charT, traits>(basic_iolist &, basic_iolist &) noexcept;
  };


  // Syntactic sugar for iolist::append().
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, const charT *const s)
  { return iol.append(s); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, const std::basic_string<charT, traits> &s)
  { return iol.append(s); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, std::basic_string<charT, traits> &&s)
  { return iol.append(std::move(s)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, const basic_iolist<charT, traits> &other)
  { return iol.append(other); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, basic_iolist<charT, traits> &&other)
  { return iol.append(std::move(other)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator+=(
    basic_iolist<charT, traits> &iol, const std::initializer_list<basic_iolist<charT, traits>> il)
  { return iol.append(il); }


  // Syntactic sugar for iolist::append().
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, const charT *const s)
  { return iol.append(s); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, const std::basic_string<charT, traits> &s)
  { return iol.append(s); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, std::basic_string<charT, traits> &&s)
  { return iol.append(std::move(s)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, const basic_iolist<charT, traits> &other)
  { return iol.append(other); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, basic_iolist<charT, traits> &&other)
  { return iol.append(std::move(other)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &operator<<(
    basic_iolist<charT, traits> &iol, const std::initializer_list<basic_iolist<charT, traits>> il)
  { return iol.append(il); }


  // Concatenate a string and an iolist, or two iolists.  Linear in the total
  // number of characters in the lvalue operands (so, constant if both operands
  // are rvalues).
  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, const charT *const s)
  { basic_iolist<charT, traits> ret(iol); ret.append(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, const std::basic_string<charT, traits> &s)
  { basic_iolist<charT, traits> ret(iol); ret.append(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, std::basic_string<charT, traits> &&s)
  { basic_iolist<charT, traits> ret(iol); ret.append(std::move(s)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, const basic_iolist<charT, traits> &other)
  { basic_iolist<charT, traits> ret(iol); ret.append(other); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, basic_iolist<charT, traits> &&other)
  { basic_iolist<charT, traits> ret(iol); ret.append(std::move(other)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const basic_iolist<charT, traits> &iol, const std::initializer_list<basic_iolist<charT, traits>> il)
  { basic_iolist<charT, traits> ret(iol); ret.append(il); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, const charT *const s)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, const std::basic_string<charT, traits> &s)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, std::basic_string<charT, traits> &&s)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(std::move(s)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, const basic_iolist<charT, traits> &other)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(other); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, basic_iolist<charT, traits> &&other)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(std::move(other)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    basic_iolist<charT, traits> &&iol, const std::initializer_list<basic_iolist<charT, traits>> il)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.append(il); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const charT *const s, const basic_iolist<charT, traits> &iol)
  { basic_iolist<charT, traits> ret(iol); ret.prepend(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const std::basic_string<charT, traits> &s, const basic_iolist<charT, traits> &iol)
  { basic_iolist<charT, traits> ret(iol); ret.prepend(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    std::basic_string<charT, traits> &&s, const basic_iolist<charT, traits> &iol)
  { basic_iolist<charT, traits> ret(iol); ret.prepend(std::move(s)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const std::initializer_list<basic_iolist<charT, traits>> il, const basic_iolist<charT, traits> &iol)
  { basic_iolist<charT, traits> ret(iol); ret.prepend(il); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const charT *const s, basic_iolist<charT, traits> &&iol)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.prepend(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const std::basic_string<charT, traits> &s, basic_iolist<charT, traits> &&iol)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.prepend(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    std::basic_string<charT, traits> &&s, basic_iolist<charT, traits> &&iol)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.prepend(std::move(s)); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> operator+(
    const std::initializer_list<basic_iolist<charT, traits>> il, basic_iolist<charT, traits> &&iol)
  { basic_iolist<charT, traits> ret(std::move(iol)); ret.prepend(il); return ret; }


  typedef basic_iolist<char> iolist;
  typedef basic_iolist<wchar_t> wiolist;
  typedef basic_iolist<char16_t> u16iolist;
  typedef basic_iolist<char32_t> u32iolist;

  extern template class basic_iolist<char>;
  extern template class basic_iolist<wchar_t>;
  extern template class basic_iolist<char16_t>;
  extern template class basic_iolist<char32_t>;
}


// PRIVATE

namespace ctp
{
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator &
    basic_iolist<charT, traits>::iterator::operator++()
  { using std::begin; using std::end; if (++m_s == end(*m_l)) { if (++m_l != m_l_end) m_s = begin(*m_l); }; return *this; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator
    basic_iolist<charT, traits>::iterator::operator++(int)
  { const iterator ret = *this; ++*this; return ret; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator &
    basic_iolist<charT, traits>::iterator::operator--()
  { using std::begin; using std::end; if (m_l == m_l_end || m_s == begin(*m_l)) m_s = end(*--m_l); --m_s; return *this; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator
    basic_iolist<charT, traits>::iterator::operator--(int)
  { const iterator ret = *this; --*this; return ret; }
  template<typename charT, typename traits> inline bool
    basic_iolist<charT, traits>::iterator::operator==(const iterator &other) const
  { return m_l == other.m_l && (m_l == m_l_end || m_s == other.m_s); }
  template<typename charT, typename traits> inline bool
    basic_iolist<charT, traits>::iterator::operator!=(const iterator &other) const
  { return !(*this == other); }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator::reference
    basic_iolist<charT, traits>::iterator::operator*() const
  { return *m_s; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator::pointer
    basic_iolist<charT, traits>::iterator::operator->() const
  { return m_s.operator->(); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::iterator::iterator(
      const typename std::list<std::basic_string<charT, traits>>::iterator l,
      const typename std::list<std::basic_string<charT, traits>>::iterator l_end,
      const typename std::basic_string<charT, traits>::iterator s)
    : m_l{l}, m_l_end{l_end}, m_s{s}
  { }

  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator &
    basic_iolist<charT, traits>::const_iterator::operator++()
  { using std::begin; using std::end; if (++m_s == end(*m_l)) { if (++m_l != m_l_end) m_s = begin(*m_l); }; return *this; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator
    basic_iolist<charT, traits>::const_iterator::operator++(int)
  { const const_iterator ret = *this; ++*this; return ret; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator &
    basic_iolist<charT, traits>::const_iterator::operator--()
  { using std::begin; using std::end; if (m_l == m_l_end || m_s == begin(*m_l)) m_s = end(*--m_l); --m_s; return *this; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator
    basic_iolist<charT, traits>::const_iterator::operator--(int)
  { const const_iterator ret = *this; --*this; return ret; }
  template<typename charT, typename traits> inline bool
    basic_iolist<charT, traits>::const_iterator::operator==(const const_iterator &other) const
  { return m_l == other.m_l && (m_l == m_l_end || m_s == other.m_s); }
  template<typename charT, typename traits> inline bool
    basic_iolist<charT, traits>::const_iterator::operator!=(const const_iterator &other) const
  { return !(*this == other); }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator::reference
    basic_iolist<charT, traits>::const_iterator::operator*() const
  { return *m_s; }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator::pointer
    basic_iolist<charT, traits>::const_iterator::operator->() const
  { return m_s.operator->(); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::const_iterator::const_iterator(
      const typename std::list<std::basic_string<charT, traits>>::const_iterator l,
      const typename std::list<std::basic_string<charT, traits>>::const_iterator l_end,
      const typename std::basic_string<charT, traits>::const_iterator s)
    : m_l{l}, m_l_end{l_end}, m_s{s}
  { }


  template<typename charT, typename traits> inline std::basic_string<charT, traits> basic_iolist<charT, traits>::flatten(const std::initializer_list<basic_iolist> il)
  { std::basic_string<charT, traits> ret; for (const auto i: il) for (const auto &s: i.m_l) ret.append(s); return ret; }

  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist() { m_l.emplace_back(); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(const charT *const s)
    { m_l.emplace_front(s); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(const charT *const s, const std::size_t n)
    { m_l.emplace_front(s, n); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(const std::basic_string<charT, traits> &s)
    { m_l.push_front(s); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(std::basic_string<charT, traits> &&s)
    { m_l.push_front(std::move(s)); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(const basic_iolist &iol): basic_iolist(iol.str()) { }
  template<typename charT, typename traits> inline basic_iolist<charT, traits>::basic_iolist(const std::initializer_list<basic_iolist> il)
    : basic_iolist(basic_iolist::flatten(il)) { }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::operator=(const basic_iolist &iol)
  { return *this = basic_iolist(iol); }

  template<typename charT, typename traits> inline void basic_iolist<charT, traits>::clear() noexcept
  { using std::begin; using std::end; m_l.front().clear(); m_l.erase(++begin(m_l), end(m_l)); }
  template<typename charT, typename traits> inline bool basic_iolist<charT, traits>::empty() const noexcept
  { return m_l.front().empty(); }
  template<typename charT, typename traits> inline void basic_iolist<charT, traits>::flatten()
  {
    using std::begin; using std::end;
    for (auto it = ++begin(m_l); it != end(m_l); it = ++begin(m_l))
    {
      m_l.front().append(*it);
      m_l.erase(it);
    }
  }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(const charT *const s)
  { m_l.back().append(s); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(const charT *const s, const std::size_t n)
  { m_l.back().append(s, n); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(const std::basic_string<charT, traits> &s)
  { m_l.back().append(s); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(std::basic_string<charT, traits> &&s)
  { if (!s.empty()) { if (m_l.front().empty()) m_l.front() = std::move(s); else m_l.push_back(std::move(s)); } return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(const basic_iolist &other)
  { return append(other.str()); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(basic_iolist &&other)
  {
    using std::end;
    if (!other.m_l.front().empty())
    {
      if (m_l.front().empty()) m_l = std::move(other.m_l);
      else m_l.splice(end(m_l), other.m_l);
    }
    return *this;
  }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::append(const std::initializer_list<basic_iolist> il)
  { return append(basic_iolist::flatten(il)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(const charT *const s)
  { if (m_l.front().empty()) m_l.front().assign(s); else m_l.emplace_front(s); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(const charT *const s, const std::size_t n)
  { if (m_l.front().empty()) m_l.front().assign(s, n); else m_l.emplace_front(s); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(const std::basic_string<charT, traits> &s)
  { if (m_l.front().empty()) m_l.front().assign(s); else m_l.push_front(s); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(std::basic_string<charT, traits> &&s)
  { if (m_l.front().empty()) m_l.front().assign(std::move(s)); else m_l.push_front(std::move(s)); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(const basic_iolist &other)
  { return prepend(other.str()); }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(basic_iolist &&other)
  {
    using std::begin;
    if (!other.m_l.front().empty())
    {
      if (m_l.front().empty()) m_l = std::move(other.m_l);
      else m_l.splice(begin(m_l), other.m_l);
    }
    return *this;
  }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::prepend(const std::initializer_list<basic_iolist> il)
  { return prepend(basic_iolist::flatten(il)); }

  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::assign(const charT *const s)
  { using std::begin; using std::end; m_l.front().assign(s); m_l.erase(++begin(m_l), end(m_l)); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::assign(const charT *const s, const std::size_t n)
  { using std::begin; using std::end; m_l.front().assign(s, n); m_l.erase(++begin(m_l), end(m_l)); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::assign(const std::basic_string<charT, traits> &s)
  { using std::begin; using std::end; m_l.front().assign(s); m_l.erase(++begin(m_l), end(m_l)); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::assign(std::basic_string<charT, traits> &&s)
  { using std::begin; using std::end; m_l.front().assign(std::move(s)); m_l.erase(++begin(m_l), end(m_l)); return *this; }
  template<typename charT, typename traits> inline basic_iolist<charT, traits> &basic_iolist<charT, traits>::assign(const std::initializer_list<basic_iolist> il)
  { assign(basic_iolist::flatten(il)); return *this; }

  template<typename charT, typename traits> inline void basic_iolist<charT, traits>::push_back(const charT c)
  { m_l.back().push_back(c); }
  template<typename charT, typename traits> inline void basic_iolist<charT, traits>::pop_back()
  { m_l.back().pop_back(); if (m_l.back().empty()) m_l.pop_back(); }
  template<typename charT, typename traits> inline charT &basic_iolist<charT, traits>::back()
  { return m_l.back().back(); }
  template<typename charT, typename traits> inline const charT &basic_iolist<charT, traits>::back() const
  { return m_l.back().back(); }

  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator basic_iolist<charT, traits>::begin()
  { using std::begin; using std::end; return iterator(begin(m_l), end(m_l), begin(*begin(m_l))); }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::iterator basic_iolist<charT, traits>::end()
  { using std::end; return iterator(end(m_l), end(m_l), typename std::basic_string<charT, traits>::iterator()); }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator basic_iolist<charT, traits>::begin() const
  { using std::begin; using std::end; return const_iterator(begin(m_l), end(m_l), begin(*begin(m_l))); }
  template<typename charT, typename traits> inline typename basic_iolist<charT, traits>::const_iterator basic_iolist<charT, traits>::end() const
  { using std::end; return const_iterator(end(m_l), end(m_l), typename std::basic_string<charT, traits>::const_iterator()); }

  template<typename charT, typename traits> inline std::basic_string<charT, traits> basic_iolist<charT, traits>::str() const &
  { std::basic_string<charT, traits> ret; for (const auto &s: m_l) ret.append(s); return ret; }
  template<typename charT, typename traits> inline std::basic_string<charT, traits> basic_iolist<charT, traits>::str() &&
  { flatten(); return std::move(m_l.front()); }

  template<typename charT, typename traits> inline std::basic_ostream<charT, traits> &operator<<(std::basic_ostream<charT, traits> &os, const basic_iolist<charT, traits> &iol)
  { for (const auto &s: iol.m_l) os << s; return os; }

  template<typename charT, typename traits> inline void swap(basic_iolist<charT, traits> &lhs, basic_iolist<charT, traits> &rhs) noexcept
  {
    using std::swap;
    swap(lhs.m_l, rhs.m_l);
  }
}

#endif
