#include "iolist.h"

template class ctp::basic_iolist<char>;
template class ctp::basic_iolist<wchar_t>;
template class ctp::basic_iolist<char16_t>;
template class ctp::basic_iolist<char32_t>;
