CPPFLAGS = -std=c++11 -Wall -Wextra -Werror -Os

OBJS = iolist.o

iolist.a: $(OBJS)
	ar rcs $@ $(OBJS)

iolist.o: iolist.h
